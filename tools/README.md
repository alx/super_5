Ceci est ensemble de petits outils qui nous ont été utiles pour développer ces attractions:

- connaitreAdrMac.py = un petit programme à téleverser sur une esp32 pour connaitre son adresse mac.
- tester leds = parce qu'on a toujours besoin de vérifier que c'est pas notre branchement qui couille mais bien le grand programme!
- exit_auto_start = bout de programme qui permet d'ajouter un bouton afin de ne pas bloquer une ESP32 avec un programme qui tournerait à l'infini avec un démarrage automatique, et ainsi toujours avoir une option pour retrouver l'acces au code.
- test_relai = programme pour tester le branchement du relai.
