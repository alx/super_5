from hx711 import HX711
from asyncio import sleep_ms


class AIOScales(HX711):
    def __init__(self, d_out, pd_sck):
        super(AIOScales, self).__init__(d_out, pd_sck)
        self.offset = 0

    def reset(self):
        self.power_off()
        self.power_on()

    def read_positif(self):
        return (self.read() * -1) // 1000

    def tare(self):
        self.offset = self.read_positif()

    def raw_value(self):
        return self.read_positif() - self.offset

    async def stable_value(self, reads=10, delay_us=500):
        values = []
        for _ in range(reads):
            values.append(self.raw_value())
            await sleep_ms(delay_us)
        return self._stabilizer(values)

    @staticmethod
    def _stabilizer(values, deviation=10):
        weights = []
        for prev in values:
            if prev == 0:
                prev = 1
            weights.append(sum([1 for current in values if abs(prev - current) / (prev / 100) <= deviation]))
        return sorted(zip(values, weights), key=lambda x: x[1]).pop()[0]


if __name__ == "__main__":
    scales = AIOScales(d_out=15, pd_sck=4)
    scales.tare()
    val = scales.stable_value()
    print(val)
    scales.power_off()
