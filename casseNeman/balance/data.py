from machine import Pin

lst_led = [4, 16, 17]

led_orange = Pin(lst_led[0], Pin.OUT)
led_verte = Pin(lst_led[1], Pin.OUT)
led_rouge = Pin(lst_led[2], Pin.OUT)

leds_attente = {led_verte: [0, 0, 0, 0, 0, 0],
                led_orange: [0, 1, 0, 1, 0, 1],
                led_rouge: [1, 0, 0, 1, 0, 0]}

leds_attente_peser = {led_verte: [0, 0, 0, 0, 0, 0],
                      led_orange: [0, 1, 0, 1, 0, 1],
                      led_rouge: [1, 0, 0, 1, 0, 0]}

leds_peser = {led_verte: [1, 1, 1, 1, 1, 1],
              led_orange: [1, 1, 0, 1, 1, 1],
              led_rouge: [0, 0, 0, 0, 0, 0]}

leds_attente_tire_volant = {led_verte: [0, 0, 0, 0, 0, 0],
                            led_orange: [0, 1, 0, 1, 0, 1],
                            led_rouge: [1, 1, 1, 1, 1, 1]}

leds_tire_volant = {led_verte: [0, 0, 0, 0, 0, 0],
                    led_orange: [0, 1, 0, 1, 0, 1],
                    led_rouge: [1, 1, 1, 1, 1, 1]}

leds_afficher_score_fixe = {led_verte: [1, 1, 1, 1, 1, 1],
                            led_orange: [1, 0, 1, 0, 1, 0],
                            led_rouge: [1, 1, 1, 1, 1, 1]}

leds_afficher_score_clignotant = {led_verte: [1, 1, 1, 1, 1, 1],
                                  led_orange: [1, 0, 1, 0, 1, 0],
                                  led_rouge: [1, 1, 1, 1, 1, 1]}
