import asyncio

async def bar(nb):
    count = 0 + nb
    while count < 10 + nb:
        print(f"avant await {count}")
        await asyncio.sleep(.1)
        print(f"apres le await {count}")
        count += 1
    return f"!!!!! bar terminado {nb} !!!!!"
async def spam(mot):
    print(f"debut spam {mot}")
    await asyncio.sleep(.1)
    print(f"fin de spam {mot}")
    return "!!!!!  spam terminado  !!!!!"

async def main():
    tache_bar = asyncio.create_task(bar(0))
    return_spam = await spam("parapluie")
    tache_bar_10 = asyncio.create_task(bar(10))
    await asyncio.sleep(5)
    return tache_bar.result(), tache_bar_10.result(), return_spam


print(asyncio.run(main()))