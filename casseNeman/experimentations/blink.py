from machine import Pin, PWM
from time import sleep
import random


F_MIN = 1
F_MAX = 1000

f = F_MIN
delta_f = 1

p = PWM(Pin(2), f)
print(p)

while True:
    p.freq(f)

    sleep(10 / F_MIN)

    f += delta_f
    if f >= F_MAX or f <= F_MIN:
        delta_f = -delta_f
