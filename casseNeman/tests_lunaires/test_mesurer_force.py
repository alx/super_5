import asyncio
from aioscales import AIOScales
from machine import Pin
import time
from functions import *
from classJeu import *


dt = Pin(13) # data de la balance
sck = Pin(12) #clock de la balance

j = Jeu()
balance = AIOScales(dt, sck)


async def main():
    balance.offset()
    print("met  la bouteille")
    time.sleep(3)
    print("valeur de 1,15kg")
    valeur1_15=balance.read()*-1
    print(valeur1_15)
    time.sleep(2)
    print("debut")
    for _ in range(10):
        j.step = "peser"
        print(f"tare = {balance.offset}, raw_value {balance.raw_value()} ({balance.read()})\n")
        # await mesurer_poids(balance, j)
        # print(_, "-->", j.poids)
        time.sleep(0.1)
        j.poids = 0
    
    
asyncio.run(main())

# aurélien 568
# Alice 542
# Boujou
# Alexandre 