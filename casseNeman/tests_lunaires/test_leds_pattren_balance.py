import asyncio
from functions import *
from data import *
from aioscales import *
from classJeu import Jeu
from machine import Pin
import time

dt = Pin(13)  # data de la balance
sck = Pin(12)  # clock de la balance



j = Jeu()
balance = AIOScales(dt, sck)


async def main():
    asyncio.create_task(led_balance_pattern(j, j.tempo))
    balance.tare()
    # print("tarre faite")
    time.sleep(2)
    print("debut")
    for s in list_step:
        j.step = s
        
        print(j.tempo)
        #print(f"\nstep = {j.step}")
        #await mesurer_poids(balance, j)
        #print(f"poids = {j.poids}")
        j.poids = 0
        await asyncio.sleep(5)
        j.tempo += 1

asyncio.run(main())