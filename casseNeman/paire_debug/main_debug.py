import asyncio
import network
import aioespnow
from functions import recoie
from classJeu import Jeu
from machine import Pin


# initialisation du réseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)

# ajout des peers
# à  modifier avec les bonnes adresses MAC!!!
bcast = b'\xff' * 6  # brodcast
esp.add_peer(bcast)
peer_phare = b'\xe8k\xea\xc4j\x80'
esp.add_peer(peer_phare)
peer_balance = b'\xe4e\xb8\xa2\xccD'
esp.add_peer(peer_balance)
peer_volant = b'\xe4e\xb8\xa2\xe0\xe0'
esp.add_peer(peer_volant)
list_peers = [peer_phare, peer_balance, peer_volant]
print("table des peers :", esp.peers_table)

async def main():
    pass




pin_bt = 15
bt_esc_auto_boot = Pin(pin_bt, Pin.IN, Pin.PULL_UP)

if not bt_esc_auto_boot.value():
    print("finitos le programme qui tourne en rond\n boot_bt_value : ", bt_esc_auto_boot.value())
else:
    asyncio.run(main(True))