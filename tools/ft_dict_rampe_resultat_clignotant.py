def fabrique_dict_afficher_score_clignotant(score: int, list_leds_rampe: list, time=3,
                                            tempo=0.5):  # temps: 3s tempo: 0.5s
    """fonction qui retourne un dictionnaire adapté au score
    necessite de lui passer le score, une liste de leds formatées en pin
    """

    def rampe_afficher_score_clignotant(res=score, nbr_of_led=len(list_leds_rampe), t=time,
                                        tp=tempo):  # temps: 3s tempo: 0.5s
        """fonction qui génère la liste correspondant au score pour l'allumage des leds
        reprends les paramètres passés à la fonction supérieure
        retourne une liste de liste"""
        grand_lst = []
        for led in range(nbr_of_led):
            ptite_lst = []
            for tour in range(int(t // tp)):
                if tour % 2 != 0:
                    ptite_lst.append(int(led < res + 1))
                else:
                    ptite_lst.append(0)
            grand_lst.append(ptite_lst)
        return grand_lst

    leds_afficher_score_clignotant = {}
    tableau_a_afficher = rampe_afficher_score_clignotant()

    for ind, lampe in enumerate(list_leds_rampe):
        leds_afficher_score_clignotant[lampe] = tableau_a_afficher[ind]

    return leds_afficher_score_clignotant


if __name__ == "__main__":
    liste_de_led_formater_en_Pin = ["led1", "led2", "led3", "led4", "led5", "led6", "led7", "led8"]
    for i in range(8):
        tab = fabrique_dict_afficher_score_clignotant(i, liste_de_led_formater_en_Pin)
        print(tab)
