Dans ce dossier vous trouverez pour le moment:
- un dossier de tests d'experimentation, où vous pourrez trouver nos pérégrination dans le monde du micropython, asyncio et autre joyeuseté. Désolées, les noms ne sont pas toujours explicites et les programmes pas forcément fonctionnels, mais c'est vraiment un dossier de recherche.. (On imagine que si on était des génies en Git, on aurait une branche pour ça mais on y est pas encore!
- un dossier balance, qui contient tous les fichiers nécessaires à télécharger sur la carte ESP32. 
- un dossier volant, idem
- un dossier phare, idem
- un dossier commun, qui contient les fichiers communs à toutes les attractions (qui sont ensuite dupliqué dans chaque dossier. (c'est un dossier de travail plutot)
- un dossier tests_lunaires, qui contient l'ensemble des programmes de tests executé pour éprouver nos fonctions et les améliorer


# attention
Ceci est un travail en cours, par exemple dans les dossiers balance, volant et phare ne contiennent pas de boot.py ni de main.py, parce que pour le moment on travaille pas avec des esp32 autonome. Donc si vous voulez utiliser ces codes, adaptez les!

