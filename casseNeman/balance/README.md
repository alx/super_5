# Presentation de la balance
La balance est une esp32 connectée à des capteurs de forces et à des leds. 
Les leds ont des patterns différents selon le moment du jeu. 
Elle communique et synchronise sa progression avec les autres parties du jeu grâce au protocole ESPNOW.

# hardware
La balance est constituée d'une ESP32 flashée avec micropython. 
Elle est reliée à des jauges de contrainte grâce à une HX711, ainsi qu'à des leds via des MOFSET IRF520.
Nous avons installé un bouton supplémentaire pour permettre de sortir du programme afin de pouvoir reflasher la carte en cas d'erreur, sans perdre les données à l'interieur.
Elle est alimentée par des piles, afin qu'elle soit autonome. 
 


# sommaire de ce dossier
Ce dossier comprends les différentes librairies nécessaires à la fabrication du module de la balance.

- HX711.py =  module python pour utiliser la puce HX711, necessaire pour utiliser des capteurs de forces.
- aioscales.py = un module asynchrone de HX711, qui retourne des valeurs positives (à l'inverse du module HX711.py) et divisée par 1000 afin de pouvoir comprendre ce qu'on fait.
- classJeu.py = une classe Jeu, qui permet de gérer les étapes du jeu, les tempo d'affichage des leds, les score, etc. Et qui nous permet de partager des informations entre les modules et les fonctions de manière globale.
- functions.py =  commun probablement à tous les modules (puisqu'ils utiliseronts des fonctions communes) est l'ensemble des fonctions utilisés pour faire fonctionner la balance.
- data_balance.py = contient tous les patterns de lumières, les constantes necessaires au jeu.
- main_balance.py = le jeu lui même



# installation

