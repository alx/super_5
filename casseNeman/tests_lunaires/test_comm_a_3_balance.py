import machine
import utime
import functions
from machine import Pin
from classJeu import Jeu
import network
import aioespnow
import asyncio

gpio_led_builtin = 2
bt_esc_auto_boot = Pin(4, Pin.IN, Pin.PULL_UP)
led_pin = Pin(gpio_led_builtin, Pin.OUT)

j = Jeu()
sta = network.WLAN(network.STA_IF)
sta.active(True)
esp = aioespnow.AIOESPNow()
esp.active(True)
peer_volant = b'\xe4e\xb8\xa2\xe0\xe0'
peer_rampe = b'\xb0\xa72)\x84`'
peer_bcast = b'\xff' * 6
esp.add_peer(peer_bcast)
esp.add_peer(peer_rampe)
esp.add_peer(peer_volant)


async def main():
    
    asyncio.create_task(functions.recoie(esp, j))
    await asyncio.sleep(6)
    j.force = 78
    await functions.envoie(esp, j)
    await asyncio.sleep(10)
    print(j.step, " ", j.score, " ",j.poids, " ", j.force)
    

if not bt_esc_auto_boot.value():
    print("end of the programe : boot_bt_value : ", bt_esc_auto_boot.value())
else:
    asyncio.run(main())
            