import asyncio
import network
import aioespnow
from classDebug import Debug
from classJeu import Jeu

# initialisation du réseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)

bcast = b'\xff' * 6  # brodcast
jeu = Jeu()

async def main():
    db = Debug(debuggeur = True, addr = bcast, espnow = esp, jeu = jeu)
    await db.initie_debug()

    asyncio.create_task(db.recoit())
    while True:
        await asyncio.sleep(0)


if __name__== "__main__":
    asyncio.run(main())