import asyncio

async def bar(char):
    count = 0
    while count < 10:
        print(f"{char} => {count}")
        await asyncio.sleep(.0)  # Pause 1s
        count += 1
    return f"bar{char}"

async def printB(truc):
    print("truc")
    await asyncio.sleep(0)
    print("apres truc")
    return 6


async def main():
    task_bar1 = asyncio.create_task(bar("a"))
    nb = await printB("la premiere est lancée")
    task_bar2 = asyncio.create_task(bar("b"))
    await asyncio.sleep(1)
    return "ok", nb, task_bar2.result()

print(asyncio.run(main()))