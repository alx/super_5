import asyncio
import cv2 as cv
import base64

import numpy
import numpy as np


ip = "192.168.234.17"
port = 5000
CHUNK = 1228800
async def receive(reader):
    pass


async def main():
    print("debut")
    r, w = await asyncio.open_connection(ip, port)
    cv.namedWindow('Receiver', cv.WINDOW_NORMAL)
    cv.resizeWindow('Receiver', 640, 480)
    i = 0
    while True:
        i += 1
        print("ok -", i)
        buffer = base64.b64encode(b'')
        while len(buffer) < 1228800:
            data_64 = await r.read(CHUNK)
            img_np = base64.b64decode(data_64)
            buffer += img_np
        #print("    ", img_np)
        #frame = np.zeros((480, 640, 3), dtype=np.uint8)
        img = np.frombuffer(buffer, dtype=np.uint8)
        print(img.shape)
        img.shape = (480, 640, 3)

        print(img)

        cv.imshow('Receiver', img)
        await asyncio.sleep(0.01)

asyncio.run(main())


