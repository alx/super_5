import asyncio
from aioscales import AIOScales
from machine import Pin
import time
from functions import *
from classJeu import *
import network


dt = Pin(13) # data de la balance
sck = Pin(12) #clock de la balance

sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)
peer=b'\xe4e\xb8\xa2\xe0$'
peer2=b'\xff' * 6
esp.add_peer(peer)
esp.add_peer(peer2)


j = Jeu()
balance = AIOScales(dt, sck)

time.sleep(1)

balance.tare()

time.sleep(1)

for i in range(5):
    print("pour voir : ", balance.raw_value())

print("tare =", balance.offset)
time.sleep(1)

async def main():
    for s in list_step:
        j.step = s
        print("step  = ", s)
        for i in range(15):
            res = await mesurer_poids(balance, j)
            print(f" poids = {j.poids}, step = {j.step}, force = {j.force}, res = {res}")
            await envoie(esp, j) 
            time.sleep(0.5)
            if j.step == "tire-volant":
                j.step = s

    
asyncio.run(main())

    