Using the libcamera documentation at https://www.raspberrypi.com/documentation/computers/camera_software.html#libcamera-vid
Maybe stream from the pi (e.g. 192.168.1.90) using TCP over the network 
  ->  libcamera-vid -t 0 --inline --listen -o tcp://0.0.0.0:8554
On a PC/pi/ipad/laptop on the same network, using VLC, Open Network Stream 
  ->  tcp/h264://192.168.1.90:8554


opencv pour lire un stream:
   -> cv2.Videocapture(URL + "81/stream")


