#########
# PHARE #
#########



import asyncio
import network
import aioespnow
import random
from machine import Pin
from aioscales import AIOScales
from classJeu import Jeu
from functions import (recoie, envoie, light_pattern)
from classDebug import Debug

# initi reseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

# init ESP-NOW
esp = aioespnow.AIOESPNow()
esp.active(True)

# ajout des peers
bcast = b'\xff' * 6
esp.add_peer(bcast)
peer_volant = b'\xe4e\xb8\xa2\xe0\xe0'
esp.add_peer(peer_volant)
peer_balance = b'\xe4e\xb8\xa2\xccD'
esp.add_peer(peer_balance)

# Initialisation des broches des leds
lst_broche = [32, 33, 25, 26, 5, 17, 16, 4]
lst_leds = [Pin(broche, Pin.OUT) for broche in lst_broche]

#Initialisation de la broche de l'electo-aimant
aimant = Pin(14, Pin.OUT)
aimant.value(1)

#Initialisation du bouton de ré-aimantage et bouton mode
bouton_aimant = Pin(12, Pin.IN, Pin.PULL_UP)
bouton_mode = Pin(13, Pin.IN, Pin.PULL_UP)


async def main(mod_test=False):
    j = Jeu("phare")
    db = Debug(debuggeur=False, addr=b'adresse du pair debuggeur', espnow=esp, jeu = j)
    db.print(f'{j.list_step}')
    asyncio.create_task(recoie(esp, j))
    asyncio.create_task(light_pattern(j))
    asyncio.create_task(j.change_mode(bouton_mode, esp))
    while True:
        db.print(f"{j.step}, score = {j.score}, mode = {j.mode}")
        if j.step == "afficher-score-fixe":
            if (j.mode == True) and (j.score == 8):
                aimant.value(0)
            db.print("affiche score fixe")
            await asyncio.sleep((j.score + 2) * j.tempo)
            j.avance()
            db.print("affiche score clignotant")
            await envoie(esp, j)
            j.fabrique_dict_afficher_score_clignotant(j.score + 1, lst_leds)
            db.print(f"{j.liste_score_clignotant}")
            await asyncio.sleep(4 * j.tempo)
            await envoie(esp, j)
            j.avance()
            db.print("affiche attente")
            await envoie(esp, j)
        elif j.step == "attente_aimant":
            db.print(f"attente_aimant, mode = {j.mode}, score = {j.score}")
            if (j.mode == True) and (j.score == 8): 
                db.print(f"val = {val} et prec = {prec}")
                while bouton_aimant.value() == 1:
                    db.print("j'attend la consigne")
                    await asyncio.sleep(0.01)
                aimant.value(1)
                db.print("j'accroche l'aimant")
            j.avance()
            await envoie(esp, j)
        else:
            if mod_test:
                j.avance()
                if j.step == "tire-volant":
                    j.score = random.randint(8,8)
                db.print(f"step = {j.step}, score = {j.score}")
                await asyncio.sleep(5)
            await asyncio.sleep(0)

pin_bt = 2
bt_esc_auto_boot = Pin(pin_bt, Pin.IN, Pin.PULL_UP)

if bt_esc_auto_boot.value():
    print("finitos le programme qui tourne en rond\n boot_bt_value : ", bt_esc_auto_boot.value())
else:
    asyncio.run(main())