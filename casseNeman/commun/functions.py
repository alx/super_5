import asyncio
from aioscales import AIOScales
from classJeu import Jeu
from data import *


async def envoie(espnow, jeu: Jeu, list_peer=[b'\xff' * 6]):
    """"coroutine qui envoie un message en broadcast
    (c'est à dire à tous les pairs précedement ajoutés)
    """
    msg = f"{jeu.step},{jeu.poids},{jeu.force},{jeu.score},{jeu.tempo},{jeu.mode}"
    # print(f"envoie : step = {jeu.step}, force = {jeu.force}, score = {jeu.score}, mode = {jeu.mode}")
    # print("msg envoyer : ", msg)
    for peer in list_peer:
        await espnow.asend(peer, msg)
        # await asyncio.sleep_ms(20)


async def recoie(espnow, jeu: Jeu, db):
    """coroutine, utilisée en tache, qui reçoie les messages des autres éléments du jeux et qui les range
    correctement dans l'instance jeu, dans le bon type de données."""
    while True:
        peer, msg = await espnow.arecv()
        if msg=="debug":
            jeu.debug = True
        else:
            jeu.step, poids, force, score, tempo, mode = msg.decode().split(',')
            jeu.poids = int(poids)
            jeu.force = int(force)
            jeu.score = int(score)
            jeu.tempo = float(tempo)
            if jeu.module == "phare":
                if jeu.mode != mode:
                    await envoie(espnow, jeu)
            else:
                jeu.mode = mode
        db.print(f"recoit : step = {jeu.step}, force = {jeu.force}, score = {jeu.score}, mode = {jeu.mode}, debug = {jeu.debug}")


async def mesurer_force(hx711: AIOScales, jeu: Jeu, coeff):
    valeur_max = 0
    max = jeu.calcul_max(coeff_jeu=coeff)
    for _ in range(15):
        valeur = hx711.raw_value()
        if jeu.mode and valeur > max:  # si la valeur depasse celle correspondant au score de 8
            jeu.calcule_score(valeur, coeff_jeu=coeff)
            jeu.avance()
            return
        elif valeur > valeur_max:
            valeur_max = valeur
        await asyncio.sleep(0.1)
    # print(f"valeur_max: {valeur_max}")
    jeu.calcule_score(valeur_max, coeff_jeu=coeff)
    # print(jeu.score)
    jeu.avance()
    return


def tare(hx711: AIOScales):
    hx711.tare()


async def declenche_mesurer_poids(hx711: AIOScales, jeu: Jeu, espnow, list_peer, seuil: int = 200):
    """fonction qui permet de passer de l'etape attente à l'etape attente-peser quand la balance dépasse 100
    puis qui passe à l'etape peser quand la valeur calculé est plus grande que la précedente
    """
    value_prec = 0
    # print("debut fonction decl mesurer poids")
    while jeu.step == jeu.list_step[0]:  # attente
        value_prec = hx711.raw_value()
        await asyncio.sleep(1)
        # print("valeur balance : ",value_prec)
        if value_prec > seuil:
            jeu.avance()
            await envoie(espnow, jeu, list_peer)
            await asyncio.sleep(1)
            # print("fin de ", jeu.step)
        else:
            await asyncio.sleep(0)

    while jeu.step == jeu.list_step[1]:  # attente-peser
        # print("step : ", jeu.step)
        raw_value = hx711.raw_value()
        # print("valprec: ", value_prec, " ", "rawval :", raw_value)
        if raw_value > seuil:
            # print("fin de :", jeu.step)
            jeu.avance()
        else:
            await asyncio.sleep(0)


async def mesurer_poids(hx711: AIOScales, jeu: Jeu):
    """coroutine qui calcule une valeur moyenne sur 8 secondes
    change jeu.poids et change de step"""
    jeu.poids = await hx711.stable_value(16, 500)
    if jeu.poids < 0:
        jeu.poids *= -1
    jeu.avance()  # "attente-tire-volant"


async def declenche_tire_volant(hx711: AIOScales, jeu: Jeu):
    while jeu.step == "attente-tire-volant":
        # print("volant = ", hx711.raw_value())
        if hx711.raw_value() > 50:
            # print("volant = ", hx711.raw_value())
            jeu.avance()
        await asyncio.sleep(0)


async def light_read_sample(dict_leds_step, j):
    taille_tableau = -1
    for led, valeur in dict_leds_step.items():
        led.value(not valeur[j])  # not pour le relai et pas pour la balance
        if taille_tableau == -1:
            taille_tableau = len(valeur)
        # print(f"{j}:{led}:{valeur[j]}")
        await asyncio.sleep(0)
    j = (j + 1) % taille_tableau
    return j


async def light_pattern(jeu: Jeu):
    i = 0
    step_prec = jeu.step
    while True:
        if jeu.step != step_prec:
            i = 0
        # print(f"etape = {i}, step = {jeu.step}")
        if (jeu.step == jeu.list_step[0]) or (jeu.step == jeu.list_step[7]):  # "attente":
            i = await light_read_sample(leds_attente, i)
        elif jeu.step == jeu.list_step[1]:  # "attente-peser":
            i = await light_read_sample(leds_attente_peser, i)
        elif jeu.step == jeu.list_step[2]:  # "peser":
            i = await light_read_sample(leds_peser, i)
        elif jeu.step == jeu.list_step[3]:  # "attente-tire-volant":
            i = await light_read_sample(leds_attente_tire_volant, i)
        elif jeu.step == jeu.list_step[4]:  # "tire-volant":
            i = await light_read_sample(leds_tire_volant, i)
        elif jeu.step == jeu.list_step[5]:  # "afficher-score-fixe":
            i = await light_read_sample(leds_afficher_score_fixe, i)
        elif jeu.step == jeu.list_step[6]:  # "afficher-score-clignotant":  # tricks espace de nomage
            i = await light_read_sample(jeu.liste_score_clignotant, i)
        step_prec = jeu.step
        await asyncio.sleep(jeu.tempo)

