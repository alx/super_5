import asyncio
from asyncio import Lock

async def task(i, lock):
    while 1:
        await lock.acquire()
        print("Acquired lock in task", i)
        await asyncio.sleep(0.5)
        lock.release()

async def main():
    lock = Lock()  # The Lock instance
    tasks = [None] * 3  # For CPython compaibility must store a reference see Note
    for n in range(1, 4):
        tasks[n - 1] = asyncio.create_task(task(n, lock))
    await asyncio.sleep(10)

asyncio.run(main())