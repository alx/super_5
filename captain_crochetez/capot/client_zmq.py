import zmq
import cv2
import numpy as np
import base64

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://192.168.234.17:5000")  # Replace 'sender_ip' with the actual sender's IP

socket.setsockopt_string(zmq.SUBSCRIBE, '')

while True:
    jpg_as_text = socket.recv()
    jpg_original = base64.b64decode(jpg_as_text)
    jpg_as_np = np.frombuffer(jpg_original, dtype=np.uint8)
    frame = cv2.imdecode(jpg_as_np, flags=1)

    cv2.imshow('Receiver', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):  # Press 'q' to exit
        break

cv2.destroyAllWindows()