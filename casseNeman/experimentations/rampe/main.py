## materiel definition I2S
i2s_id = 1
sck_pin = Pin(15)
ws_pin = Pin(4)
sd_pin = Pin(2)
buffer_lengh = 2200
## materiel definition i2s
buf = bytearray(128)
buf_mv = memoryview(buf)

audio_out = I2S(i2s_id,
                sck=sck_pin,
                ws=ws_pin,
                sd=sd_pin,
                mode=I2S.TX,
                bits=32,
                format=I2S.MONO,
                rate=44100,
                ibuf=20000)

async def continuous_play(audio_out):
    global buf_mv
    swriter = asyncio.StreamWriter(audio_out)

    while True:
        if len(buf_mv) > 1024:
            swriter.out_buf = buf_mv[:1024]
            await swriter.drain()
            
async def attente_message():
    print("je me lance")
    global buf_mv
    swriter = asyncio.StreamWriter(audio_out)
    while True:
        host, val = await esp.arecv()
        #print(f"message = {val}, {len(val)}")
        a = memoryview(val)
        swriter.out_buf = a[:128]
        await swriter.drain()
        print(bytes(a))
        #buf_mv += a
        #print(bytes(buf_mv))
     
     
async def main(audio_out):
    #play = asyncio.create_task(continuous_play(audio_out))
    task_a = asyncio.create_task(attente_message())
    # keep the event loop active
    while True:
        await asyncio.sleep_ms(10)
        
   


try:
    asyncio.run(main(audio_out))
    
except (KeyboardInterrupt, Exception) as e:
    print("Exception {} {}\n".format(type(e).__name__, e))
    
finally:
    # cleanup
    print(buf)
    audio_out.deinit()
    ret = asyncio.new_event_loop()  # Clear retained uasyncio state
    print("==========  DONE PLAYBACK ==========")