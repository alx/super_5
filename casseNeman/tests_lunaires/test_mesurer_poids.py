import asyncio
from aioscales import AIOScales
from machine import Pin
import time
from functions import *
from classJeu import *


dt = Pin(13) # data de la balance
sck = Pin(12) #clock de la balance

j = Jeu()
balance = AIOScales(dt, sck)


async def main():
    balance.tare()
    print("monte")
    time.sleep(2)
    print("debut")
    for _ in range(10):
        j.step = "peser"
        print(f"tare = {balance.offset}, raw_value {balance.raw_value()}")
        await mesurer_poids(balance, j)
        print(_, "-->", j.poids)
        time.sleep(0.5)
        j.poids = 0
    
    
asyncio.run(main())