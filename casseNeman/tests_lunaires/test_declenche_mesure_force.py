import asyncio
from functions import *
from data import *
from aioscales import *
from classJeu import Jeu
from machine import Pin
import time

dt = Pin(13)  # data de la balance
sck = Pin(12)  # clock de la balance



j = Jeu()
balance = AIOScales(dt, sck)


async def main():
    asyncio.create_task(led_balance_pattern(j, 0.3))
    balance.tare()
    # print("tarre faite")
    time.sleep(2)
    print("debut")
    for _ in range(15):
        j.step = "attente-tire-volant"
        print(f"\nstep = {j.step}")
        await declenche_mesure_force(balance, j)
        print(f"stp = {j.step}")
        j.poids = 0
        await asyncio.sleep(1)

asyncio.run(main())