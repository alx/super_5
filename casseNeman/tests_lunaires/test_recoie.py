import asyncio
from aioscales import AIOScales
from machine import Pin
import time
import aioespnow
from functions import recoie
from classJeu import Jeu
import network



sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)
bcast = b'\xff' * 6
esp.add_peer(b'\xe4e\xb8\xa2\xe0\xe0')



j = Jeu()
time.sleep(1)

async def main():
    asyncio.create_task(recoie(esp, j))
    poids_recu = 1
    while True:
        await asyncio.sleep(0.1)
        if poids_recu != j.poids:
            print(f"nouveau poids = {j.poids},  de type : {type(j.poids)}")
            poids_recu=j.poids
        else:
            # print("rien reçu!")
            await asyncio.sleep(0)

    
asyncio.run(main())

    
