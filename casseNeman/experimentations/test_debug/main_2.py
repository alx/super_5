import asyncio
import network
import aioespnow
from classDebug import Debug
from classJeu import Jeu
from functions import envoie, recoit

# initialisation du réseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)

bcast = b'\xff' * 6  # brodcast
peer = b''
jeu = Jeu()
db = Debug(debuggeur=False, addr=None, espnow=esp)


async def main():
    compteur = 0
    asyncio.create_task(recoit(esp, jeu, db))
    while True:
        compteur += 1
        envoie(esp, jeu, list_peer)