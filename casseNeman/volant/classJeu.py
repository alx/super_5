from collections import OrderedDict

class Jeu:
    list_step = ["attente", "attente-peser", "peser", "attente-tire-volant",
                 "tire-volant", "afficher-score-fixe", "afficher-score-clignotant"]

    list_tempo = [0.5, 1, 1, 0.5, 0.2, 1, 0.5]

    def __init__(self, force=0, poids=-1, force_max_possible: int = 500):
        self.step = self.list_step[0]
        self.force = force
        self.poids = poids
        self.force_max_possible = force_max_possible
        self.tempo = self.list_tempo[0]
        self.score = 0
        self.liste_score_clignotant = OrderedDict()

    def avance(self):
        ind_step = -1
        for i in range(len(self.list_step)):
            if self.list_step[i] == self.step:
                #print("classJeu : i=", i)
                ind_step = i
        self.step = self.list_step[(ind_step + 1) % 7]
        self.tempo = self.list_tempo[(ind_step + 1) % 7]

    def calcule_score(self, valeur, coeff_jeu=0.02):
        div = self.poids * coeff_jeu
        if div == 0:
            self.score = 0
        else:
            self.score = int(valeur // div)
            if self.score > 8:
                self.score = 8


    def fabrique_dict_afficher_score_clignotant(self, score: int, list_leds_rampe: list, time=3,
                                            tempo=0.5, time_sleep=3):  # temps: 3s tempo: 0.5s
        """fonction qui retourne un dictionnaire adapté au score
        necessite de lui passer le score, une liste de leds formatées en pin
        """

        def rampe_afficher_score_clignotant(self, res=score, nbr_of_led=len(list_leds_rampe), t=time,
                                        tp=tempo, t_sleep=time_sleep):  # temps: 3s tempo: 0.5s
            """fonction qui génère la liste correspondant au score pour l'allumage des leds
            reprends les paramètres passés à la fonction supérieure
            retourne une liste de liste"""
            grand_lst = []
            for led in range(nbr_of_led):
                ptite_lst = []
                for tour in range(int(t // tp)):
                    if tour % 2 != 0:
                        ptite_lst.append(int(led < res))
                    else:
                        ptite_lst.append(0)
                ptite_lst += [0 for i in range(int(t_sleep // tp))]
                grand_lst.append(ptite_lst)
            return grand_lst

        leds_aff_score_clign = {}
        tableau_a_afficher = rampe_afficher_score_clignotant(self)

        for ind, lampe in enumerate(list_leds_rampe):
            leds_aff_score_clign[lampe] = tableau_a_afficher[ind]

        self.liste_score_clignotant = leds_aff_score_clign


if __name__=="__main__":
    j = Jeu()
    j.fabrique_dict_afficher_score_clignotant(4, ["led1", "led2"], time=6, time_sleep=6)
    print(j.liste_score_clignotant)
