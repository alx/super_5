#######
# BALANCE
#######

import asyncio
import network
import aioespnow
from machine import Pin
from classJeu import Jeu
from aioscales import AIOScales
from functions import recoie, tare, mesurer_poids, envoie, declenche_mesurer_poids, light_pattern
from classDebug import Debug

# initialisation du réseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)

# ajout des peers
#    ----->>    à  modifier avec les bonnes adresses MAC!!!
bcast = b'\xff' * 6  # brodcast
esp.add_peer(bcast)
peer_phare = b'\xe8k\xea\xc4j\x80'
esp.add_peer(peer_phare)
peer_volant = b'\xe4e\xb8\xa2\xe0\xe0'
esp.add_peer(peer_volant)
list_peers = [peer_phare, peer_volant]

# Initialisation des broche des jauge de contraintes
dt = 13  # data de la balance
sck = 12  # clock de la balance

balance = AIOScales(dt, sck)
led_builtin = Pin(2, Pin.OUT)
led_builtin.off()


async def main(mod_test: bool = False):
    balance.tare()
    j = Jeu()
    db = Debug(debuggeur=False, addr=b'adresse du pair debuggeur', espnow=esp, jeu = j)
    db.print("debut du main")
    asyncio.create_task(recoie(esp, j))
    asyncio.create_task(light_pattern(j))
    while True:
        db.print("---------------step : ", j.step)
        if j.step == "attente":
            db.print(f"j.step : {j.step}")
            await declenche_mesurer_poids(balance, j, esp, list_peers)  # attente-peser puis peser
            await envoie(esp, j, list_peers)
            await mesurer_poids(balance, j)
            await envoie(esp, j, list_peers)

        elif j.step == "afficher-score-clignotant":
            db.print("tare...")
            tare(balance)
            await asyncio.sleep(3)
            if mod_test:
                j.avance()

        else:
            await asyncio.sleep(0)
            if mod_test:  # executer pour tester sans la syncro avec les autres esp32
                print("mod_test")
                j.avance()
                await asyncio.sleep(5)
                led_builtin.on()
                await asyncio.sleep(1)
                led_builtin.off()
                print("avance : fin de ", j.step)


pin_bt = 15
bt_esc_auto_boot = Pin(pin_bt, Pin.IN, Pin.PULL_UP)

if not bt_esc_auto_boot.value():
    print("finitos le programme qui tourne en rond\n boot_bt_value : ", bt_esc_auto_boot.value())
else:
    asyncio.run(main(True))