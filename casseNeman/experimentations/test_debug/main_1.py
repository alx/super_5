import asyncio
import network
import aioespnow
from classDebug import Debug
from classJeu import Jeu

# initialisation du réseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)

bcast = b'\xff' * 6  # brodcast
jeu = Jeu()