from functions import envoie, recoie
import asyncio
from classJeu import Jeu
import network
import aioespnow


sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)

broadcast=b'\xff' * 6
peer=b'\xe4e\xb8\xa2\xe0$'
peer_rampe=b'\xb0\xa72)\x84`'

esp.add_peer(peer)
esp.add_peer(broadcast)
esp.add_peer(peer_rampe)

j = Jeu()

async def main():
    asyncio.create_task(recoie(esp, j))
    await asyncio.sleep(1)
    j.poids = 22
    await envoie(esp, j)
    await asyncio.sleep(5)
    print(j.poids, j.force, j.score)
    
asyncio.run(main())
    