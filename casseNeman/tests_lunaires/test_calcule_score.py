from classJeu import Jeu
import random

j = Jeu()
poids = [i for i in range(800,3500,200)]
force = [i for i in range(50, 500, 100)]

print(f"début, score = {j.score}")
for p in poids:
    j.poids = p
    for f in force:
        j.calcule_score(f, coeff_jeu=0.05)
        print(f"{f} vs {j.poids} ---->  {j.score}")

