import time
import random
import os
from collections import OrderedDict

def fabrique_dict_from_liste(list_of_list, list_of_leds):
    dictionnaire = OrderedDict()
    for i in range(len(list_of_list)):
        dictionnaire[list_of_leds[i]] = list_of_list[i]
    return dictionnaire

def rampe_attente(nbr_of_led=8, nbr_of_sample=255):  # tempo=0.5s
    grand_lst = []
    for led in range(nbr_of_led):
        ptite_lst = []
        for tour in range(nbr_of_sample):
            random.seed(int.from_bytes(os.urandom(4), 0))
            ptite_lst.append(random.randint(0, 1))
        grand_lst.append(ptite_lst)
    return grand_lst




def rampe_peser(nbr_of_led=8, nbr_of_sample_by_sec=10, time=8):  # tempo=1s
    """a executer sur 8 secondes !!!  """
    return [[1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 0, 0],
            [1, 1, 1, 1, 1, 0, 0, 0],
            [1, 1, 1, 1, 0, 0, 0, 0],
            [1, 1, 1, 0, 0, 0, 0, 0],
            [1, 1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0]]


def rampe_attente_tire_volant(nbr_of_led=8):  # tempo=0.5s
    return [[0, 1, 1, 1, 1],
            [0, 0, 1, 1, 1],
            [0, 0, 0, 1, 1],
            [0, 0, 0, 0, 1],
            [0, 0, 0, 0, 1],
            [0, 0, 0, 1, 1],
            [0, 0, 1, 1, 1],
            [0, 1, 1, 1, 1]]


def rampe_tire_volant():  # temps d'execution 1.5s tempo=0.2s
    return [[0, 0, 0, 0, 0, 0, 1, 1],
            [0, 0, 0, 0, 1, 1, 1, 1],
            [0, 0, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 1]]


def rampe_afficher_score_fixe():  # tempo=1
    return [[0, 1, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 0, 1]]

def rampe_afficher_score_clignotant(score:int, nbr_of_led=8, time=3, tempo=0.5): # temps: 3s tempo: 0.5s
    grand_lst = []
    for led in range(nbr_of_led):
        ptite_lst = []
        for tour in range(int(time//tempo)):
            if tour % 2 != 0:
                ptite_lst.append(int(led < score))
            else:
                ptite_lst.append(0)
        grand_lst.append(ptite_lst)
    return grand_lst


if __name__ == "__main__":
    tab = rampe_attente()
    for i in tab:
        print(i)



