from machine import Pin
from light_pattern import *


lst_led = [32, 33, 25, 26, 5, 17, 16, 4]
lst_pin = [Pin(l) for l in lst_led]

leds_attente = fabrique_dict_from_liste(rampe_attente(), lst_pin)

leds_attente_peser = leds_attente

leds_peser = fabrique_dict_from_liste(rampe_peser(), lst_pin)

leds_attente_tire_volant = fabrique_dict_from_liste(rampe_attente_tire_volant(), lst_pin)

leds_tire_volant = fabrique_dict_from_liste(rampe_tire_volant(), lst_pin)

leds_afficher_score_fixe = fabrique_dict_from_liste(rampe_afficher_score_fixe(), lst_pin)




if __name__ == "__main__":
    print("leds_attente = ", leds_attente)
    print("leds_attente_peser = ", leds_attente_peser)
    print("leds_peser = ", leds_peser)
    print("leds_attente_tire_volant = ", leds_attente_tire_volant)
    print("leds_tire_volant = ", leds_tire_volant)
    print("leds_afficher_score_fixe ", leds_afficher_score_fixe)
