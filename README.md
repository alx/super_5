# super_5
*"moi je dit renault c'est bien"*

La Super 5 Foraine est une Renault super 5 de 1992, formant une petite fête foraine compacte et itinérante où la compréhension des équipements de cette voiture est mise à l’honneur dans une ambiance festive.
La voiture se déplie en quatre attractions foraines qui proposent chacune de comprendre par l’expérience, la communication en équipe, l’agilité ou la force (relative au poids) certains mécanismes élémentaires qui composent la voiture.


## Cap'tain Crochtez
un jeux d’adresse en binôme où il faudra de guider, grâce à une camera embarquée
dans la garniture des portières du coté gauche dont le flux vidéo sera retransmis sur un
écran, l’autre participant/e qui maniera un cintre métallique pour ouvrir la portière de
l’extérieur.

## Casse neman
un jeu de force : le/la participant/e doit tenter de tourner un volant fixe avec le plus de force possible.
En fonction de son poids et de la force déployée, il lui est attribué un score qui s’affiche sur la rampe de feu transformée pour l’occasion en vue-mètre.

## Faire les fils
Un jeu de reflexe où le/la participante doit retenir les étapes nécessaire pour démarer la voiture sans les clés et appuyer sur des boutons pour les retrouver dans le bon ordre.

# Concrètement
Toutes les attractions sont basées sur des esp32 flashée avec Micropython. 
Les codes sont donc écrits en micropython.
La communication entre les ESP32 est basée sur le protocole espnow avec la librairie aioespnow native dans python, qui permet de faire de l'espnow en asynchrone.
Les forces (poids pour la balance et force pour le volant) sont mesurées avec des jauges de contrainte reliée à une puce/shield: HX711, il existe une librairie python pour les hx711 disponible, et compatible avec micropython.
Nous utilisons des relais pour les phrares arrières de la voiture et des mosfet pour les leds de la balance. 


# Sommaire
Vous trouverez ici:
- un dossier par partie d'attraction
- un dossier de recherches générales 
- un dossier tools comprenant des utilitaires qui nous ont servi à la fabrication de ces attractions.
