# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
#import webrepl
#webrepl.start()
import machine
import asyncio
import network

#configuration réseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

esp = espnow.ESPNow()
esp.active(True)

#initialisation pin
pinStop = machine.Pin(27, machine.Pin.IN, machine.Pin.PULL_UP)
# I2S
sck_pin = machine.Pin(4)   # Serial clock output
ws_pin = machine.Pin(15)    # Word clock output
sd_pin = machine.Pin(2)    # Serial data output
audio_out = machine.I2S(2,
                sck=sck_pin, ws=ws_pin, sd=sd_pin,
                mode=machine.I2S.TX,
                bits=16,
                format=machine.I2S.MONO,
                rate=44100,
                ibuf=20000)
#microphone
mic = machine.ADC(machine.Pin(35))

async def lecture_son(writer, buf):
    writer.write(buf)
    await writer.drain()

async def wait_for_message(writer):
    while True:
        host, msg = await esp.arecv()
        print(f"message")
        lecture_son(writer, msg)

async def async_send_son(espnow, tab_chunk):
    while True:
        if tab_chunk:
            await espnow.asend(tab_chunk[0])
            del (tab_chunk[0])
        await asyncio.sleep_ms(20)


async def micro(m, samples):
    while True:
        samples.add(m.read())
        await asyncio.sleep(0.1)


async def main():
    ok = True
    samples = []
    swriter = asyncio.StreamWriter(audio_out)
    task_wait_for_message = asyncio.create_task(wait_for_message(swriter))
    task_micro = asyncio.create_task(micro())
    task_send_son = asyncio.create_task(async_send_son(esp, samples))
    while ok:
        asyncio.sleep(0.1)
        if pinStop.value() == 0:
            ok = False

    print("fin du programme")


asyncio.run(main())




