tare = 0
poids = 0
verrou_pesee = asyncio.Event()
verrou_attente = asyncio.Event()


async def reception_message():
    while True:
        nb = random.randint(0,1000)
        print(f"message reçu : {nb}")
        if nb%4 == 0:
            verrou_pesee.set()
        await asyncio.sleep(.5)
        
async def envoie_message():
    print("j'envoie une message")
    await asyncio.sleep(0.88)
    
async def led_attente():
    while attente.is_set():
        print("attente1")
        await asyncio.sleep(.35)
        print("attente2")
        await asyncio.sleep(.35)
        print("attente3")
        await asyncio.sleep(.35)
        
async def led_pesee():
    while verrou_pesee.is_set():
        print("pesee1")
        await asyncio.sleep(.35)
        print("pesee2")
        await asyncio.sleep(.35)
        print("pesee3")
        await asyncio.sleep(.35)
    
async def peser(poids):
    verrou_attente.clear()
    while verrou_pesee.is_set():
        await asyncio.sleep(3)
        verrou_attente.set()
        verrou_pesee.clear()
    poids = random.randint(20,100)
    print(f"poids = {poids}")
        
async def main():
    print("début ...")
    verrou_attente.set()
    asyncio.gather(reception_message(), led_attente())
    print("... fin")
    
asyncio.run(main())
        
        
    
    