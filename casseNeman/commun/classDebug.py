import asyncio

class Debug:

    def __init__(self, debuggeur : bool, addr , espnow, jeu):
        """debuggeur = true => self.addr = [mac, mac] liste des pairs du jeu
        debuggeur = False => self.addr = mac addr du debugger"""
        self.debuggeur = debuggeur
        self.addr = addr
        self.esp = espnow
        self._jeu = jeu
        

    async def print(self, debug_msg: str):
        if self._jeu.debug and not self.debuggeur:
            await self.esp.asend(self.addr, debug_msg)

    async def recoit(self):
        while True:
            peer, msg = await self.espnow.arecv()
            print(f"{peer} : {msg}")

    async def initie_debug(self):
        for i in range(5):
            for peer in self.addr:
                await self.esp.asend(peer, "debug")

