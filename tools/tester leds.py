from machine import Pin
import time

lst_led = [4, 5, 18]

led_verte = Pin(lst_led[0], Pin.OUT)
led_bleu = Pin(lst_led[1], Pin.OUT)
led_rouge = Pin(lst_led[2], Pin.OUT)


while True:
    led_bleu.value(0)
    led_rouge.value(0)
    led_verte.value(0)
    time.sleep(0.5)
    led_bleu.value(1)
    led_rouge.value(0)
    led_verte.value(0)
    time.sleep(0.5)
    led_bleu.value(0)
    led_rouge.value(1)
    led_verte.value(0)
    time.sleep(0.5)
    led_bleu.value(0)
    led_rouge.value(0)
    led_verte.value(1)
    time.sleep(0.5)
    
    
