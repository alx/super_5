import asyncio


async def envoie(host, msg):
    while True:
        print(f"message : {msg}, envoyer a {host}")
        await asyncio.sleep(2)


async def recoie():
    while True:
        print("je recois un message")
        await asyncio.sleep(2)


async def tare():
    global choix
    print("tare...")
    await asyncio.sleep(5)
    choix = "peser"


async def peser():
    global led_pattern
    global choix
    led_pattern = "peser"
    print("peser rouge...")
    await asyncio.sleep(1)
    print("peser orange...")
    await asyncio.sleep(1)
    print("peser vert...")
    await asyncio.sleep(1)
    choix = "jeux"


async def jeux_sur_volant():
    global choix
    print("jeux...")
    await asyncio.sleep(2)
    choix = "score"


async def score():
    global choix
    print("votre score est de 1024 !!!")
    await asyncio.sleep(1)
    choix = "attente"


async def led_pattern():
    while True:
        global choix
        if choix == "attente":
            print(f"led pattern :{choix}")
            await asyncio.sleep(1)
        elif choix == "peser":
            print(f"led pattern :{choix}")
            await asyncio.sleep(1)
        elif choix == "jeux":
            print(f"led pattern :{choix}")
            await asyncio.sleep(1)
        elif choix == "score":
            print(f"led pattern :{choix}")
            await asyncio.sleep(1)
        else:
            print(f"led pattern inconnue : {choix}")
            await asyncio.sleep(1)


async def main(host, msg):
    global choix
    task_envoie = asyncio.create_task(envoie(host, msg))
    task_recois = asyncio.create_task(recoie())
    task_pattern_led = asyncio.create_task(led_pattern())
    while True:
        if choix == "attente":
            print(f"while True, dans {choix}")
            await tare()
        elif choix == "peser":
            print(f"while True, dans {choix}")
            await peser()
        elif choix == "jeux":
            print(f"while True, dans {choix}")
            await jeux_sur_volant()
        elif choix == "score":
            print(f"while True, dans {choix}")
            await score()
        else:
            print("y'a un pb !!!")
            await asyncio.sleep(1)



host = "volant"
msg = "100"
choix = "attente"
asyncio.run(main(host, msg))
