#######
# VOLANT
#######

import asyncio
import network
import aioespnow
import random
from machine import Pin
from classJeu import Jeu
from aioscales import AIOScales
from functions import recoie, tare, mesurer_force, envoie, declenche_tire_volant
from classDebug import Debug

# initialisation du réseau
sta = network.WLAN(network.STA_IF)
sta.active(True)

# Initialize ESP-NOW
esp = aioespnow.AIOESPNow()  # Returns AIOESPNow enhanced with async support
esp.active(True)

# ajout des peers
# à  modifier avec les bonnes adresses MAC!!!
bcast = b'\xff' * 6  # brodcast
esp.add_peer(bcast)
peer_phare = b'\xe8k\xea\xc4j\x80'
esp.add_peer(peer_phare)
peer_balance = b'\xe4e\xb8\xa2\xccD'
esp.add_peer(peer_balance)
list_peers = [peer_phare, peer_balance]


# Initialisation des broche des jauge de contraintes
dt = Pin(13)  # data de la balance
sck = Pin(12)  # clock de la balance

volant_scale = AIOScales(dt, sck)


async def main(mod_test=False):
    j = Jeu()
    db = Debug(debuggeur=False, addr=b'adresse du pair debuggeur', espnow=esp, jeu = j)
    db.print("debut du main")
    asyncio.create_task(recoie(esp, j))
    while True:
        if j.step == "attente-tire-volant":
            db.print(f"attente_tire_volant, {j.mode}")
            await declenche_tire_volant(volant_scale, j)
            await envoie(esp, j, list_peers)
            db.print("tire_volant")
            await mesurer_force(volant_scale, j, coeff=0.01)
            await envoie(esp, j, list_peers)
            db.print(f"info envoyée: step:{j.step}, score={j.score}, tempo={j.tempo}")
        elif j.step == "peser":
            tare(volant_scale)
            db.print(f"{volant_scale.offset}")
            await asyncio.sleep(6)
            if mod_test:
                j.avance()
        else:
            if mod_test:
                j.poids = random.randint(800, 2000)
                await asyncio.sleep(2)
                j.avance()
            await asyncio.sleep(0)
        db.print(f"step = {j.step}")


pin_bt = 15
bt_esc_auto_boot = Pin(pin_bt, Pin.IN, Pin.PULL_UP)

if not bt_esc_auto_boot.value():
    print("finitos le programme qui tourne en rond\n boot_bt_value : ", bt_esc_auto_boot.value())
else:
    asyncio.run(main())
